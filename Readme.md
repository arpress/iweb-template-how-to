iWeb Template How-To Last Updated February 26, 2006

 
#How To Create...

##Custom iWeb Templates
by Suzanne Boben

This document has been adapted for print. You can find the original, along with other documents, at http://www.11mystics.com.

##Public Service Annoucement

PLEASE do not attempt to mess with your iWeb installation unless you understand right now that you WILL screw it up somewhere along the line. This is normal. You’ll be better off if you attempt this after a few cups of coffee, but as long as you back-up your work, then there’s nothing to fear. You can do this!! :)

 
##It Is Possible...
***...to create your own iWeb templates. They’ll even show up in your selection list when you create a new site or page! But this isn’t for just anyone - you either have to be really bored, ex- tremely interested in learning something new or just plain stubborn!***

I won’t tell you which category I fit into, but I did manage to dig into the gargan- tuan XML template file and tweak it to my liking... hence you have the template you’re looking at right now. I am a designer and it’s hard to just let things like this alone LOL.

I’m going to go over the entire process here in case I have any really bored, inter- ested and stubborn web mates out there. Please do not attempt to do this unless you have a significant working knowledge of XHTML and note the X cuz you’ll be working with an XML file like you’ve never seen before. I don’t know XML, but I know enough about its structure to be able to interpret it. If you can’t do this, then don’t risk messing up your iWeb templates! Wait for Apple to release the editor :)

##iWeb Theme versus iWeb Template

I originally wrote up instructions for altering your iWeb theme and I don’t want to confuse this with the term template. For my purposes, I use the word theme to de- scribe an alternate graphical style for an existing iWeb template. For example, on my iWeb Themes page, I have a few themes for the Freestyle template that you can download. These are simply a collection of images that you copy into your site’s Images folder. They essentially overwrite the existing images from the Freestyle template you used to create your original site. These images do not constitute a template however. A template contains much more than just images.

If you like the basic layout of an existing iWeb template, then perhaps just using the theme approach is the best way to generate a unique look for your web page. If you want full control over every element of the site however, then you need to con- sider templates.


 
##What Defines an iWeb Template?

There are three basic groups of stuff that define iWeb templates and they are:

Fonts, font styles and font colors Background images and supporting images Content and layout

Currently the stock iWeb templates allow you to change most of the fonts, font styles and font colors, the backround images, the content and most of the support- ing images when you create your own page. You cannot easily change core layout elements however, such as the navigation menu, its fonts or font styles, and you can’t change the header images for those templates that have them. You can’t change elements in the blog or podcast pages either.

Technically you can change the header image (see this page) but like everything listed here, you’re not actu- ally changing the template itself - you’re just changing stuff for the page you’re building. Every new page you add will have the original fonts, the original images and the original colors from the template - you have to go and change them all for every new page you build. Hence, this discussion becomes necessary. Do I just work around some of the template rules or do I create a new template all together?

 
 
##iWeb Template Guts & Other Gross Stuff

So if you think you’re ready for this, read on. Now I’m going to identify each of the template elements and give you a brief description of how they play into the overall body of an iWeb template.

####You Are Here

Provided you know this about a MAC, you can open up applications by selecting the Show Package Contents menu item when you right click or command click on the application icon. From there, the sys- tem displays the Contents folder. Open that and you’ll see a few addi- tional folders and some files. Open the Resources folder.

In the Resources folder you’ll find lots of fun stuff, but what you’re looking for here is a folder named English.lproj OR... the folder just like it but in your language. That’s right - the templates are I18N compliant! Once inside this folder you’ll find more stuff... ignore everything except for the folder named Templates.

####And Now You’re Here

Now you’ve found the source of all iWeb template data. You’d think now it’d be easy, but no, things are about to get confusing.

In the Templates folder, you should find six (6) folders, one for each of the page types such as Welcome or About Me. For the sake of this discussion, we’ll only talk about the Welcome folder. The pattern is the same for all six folders so there’s no need to mention them all.

Inside the Welcome folder, you’ll find twelve template files, one for each iWeb tem- plate. Now you’re probably asking yourself, “Huh? Why did they organize this way?” Well ponder that and send me a note when or if you figure that out. I’m asking the same thing. There’s probably a good reason, but I don’t know what it is. Regard- less, we’re now looking at the twelve various templates for the Welcome page. Still following?

####Almost There...

At this point, we’re going to be safe and make a copy of one of these to poke around in. Please select the file entitled Freestyle Welcome.webtemplate and right click or command click on it. Select Duplicate from the menu. You should now have a copy of the original. Rename it if you want, but just make sure you continue using the copied file, not the original.

Moving on... right click or command click on your copied Freestyle Welcome page template file and select Show Package Contents from the menu. Within this file you will find the magic components that make your template what it is. You should im mediately see the background images it uses, some pictures and most importantly, a file named index.xml.gz.

####Picking The Brain Apart

Index.xml.gz is a compressed file that contains Index.xml, the master template properties sheet for, in our case, the Freestyle Welcome page. Within it you will find a cluttered mess of stuff you never wished you’d seen, but ah, that’s where all the goodies are defined LOL! You can double click the compressed file to reveal Index.xml, but be careful if you go so far as to open the XML file itself. TextEdit will handle the file just fine, but loading it into a code editor right off the bat might prove unsuccessful. Because the XML within the file is not formatted, it’s technically all on one line and your code editor might choke when it tries to load the file. Mine did. View it in TextEdit and then decide if you have the patience to continue ;-)

At this point, you’re looking at the brain that powers an iWeb template, at least for one page. At this point I’ll discuss how to actually create a new template.

 

 
#Create an iWeb Template

In this section, I’ll describe all the things you need to do to create your own template that’s (please note now...) based upon an exist- ing iWeb template. I will walk you through the process of duplicating an existing iWeb template and then changing all the attributes you want. This is different from creating a template from scratch. I don’t know XML and the file is monstrous. No one at Apple created a tem- plate writing raw XML and I suspect you won’t either. Hence, we’ll make this one smidgeon easier by first duplicating an existing tem- plate and then tweaking the pages to our liking.

##Getting Started

Okay first things first - make sure you have the suggested tools to do the job. I’ll assume you have a graphics editor otherwise you likely wouldn’t be reading this. Next, make sure you have a good code editor that highlights code by color. Third, if you plan to change the colors of any text in the template, make sure you have a tool that converts RGB 255 to RGB 100 and vice versa. And last, you’ll need to re- zip the index.xml file - StuffIt’s Drop Stuff widget will do the trick.

Now, for the purpose of this discussion, I will again use the Freestyle template and its pages as examples. You can use any existing iWeb template you want, but real- ize that the names of the graphics and other elements, i.e. the header images or font styles, will be different. If you’ve gotten this far, then this minor detail won’t get in your way.

 

 
##Step 1 - Duplicate Existing Template

The first thing you need to do is decide which template to duplicate. I used Fre- estyle because that’s what I started with - it’s not better or worse than any other template, but you should base your decision on layout more than anything else be- cause that’s the one aspect of this process that you likely will not be able to alter. So pick the one with the layout you like best - everything else can be changed.

	1.	Navigate into the iWeb resource folder for your language (as described on the previous page) and open the Templates folder within it. You should now see the six page folders, one for each page type. 
Note the file TemplatesInfo.plist... we’ll come back to that file later. 

	2.	Open the About Me folder and find the template file you’ve chosen to duplicate. 
I obviously selected Freestyle About Me.webtemlate. 

	3.	Right click or command click on your selected file and select duplicate from the 
menu. 

	4.	Now rename the copied file using a name that reflects your new template style. I used Graffiti About Me.webtemplate and I’d suggest you only change the first word since that’s the word that defines the different templates. 

	5.	Now back up one folder level and select the next page folder, Blog. Follow steps 2-5 to create and name a new template file for that page. 

	6.	Continue until you’ve done this for all six page types and note the exact file names you gave to each one. You’ll need to enter them into the TemplatesInfo.plist file next.

 

 
##Step 2 - Register New Template

You technically have created a brand new template, but you’re not going to see it in the selection list quite yet. The reason is, it hasn’t been entered into the list that iWeb uses to find and display all templates. We’ll do that now.

	1.	In the same folder where you found the six page folders, you’ll also find the file named TemplatesInfo.plist. Duplicate this file and rename it with the word backup so you know it’s a backup of the original. 

	2.	Now open the original file. This is an XML file, but it’s not complex. It’s basically the instructions for finding and displaying all available iWeb templates and you need to enter the name of yours now. 

	3.	You should be able to make out a kind of hierarchy to the file. If you can’t, be very careful here or consider NOT doing this. The first high-level section is Categories and within that, there’s an entry for every page type, the first one being About Me. Within About Me, you can see an entry for every template, the first one being Baby. You need to duplicate the entry for Baby and change the duplicate version to match the info of your own template. See the example to the right. 

	4.	Continue down the XML page and make an entry for your new template pages for each category. The next one is Blog. The name within the <key> tags should always be the same - mine was Graffiti. 

	5.	Once you’ve entered your template pages into each of the categories, move fur- ther down the page to the section entitled: 
<key>sortedThemes</key> 

	6.	This section tells iWeb in which order to list the themes in your selection win- dow. Notice they’re not alphabetical - you can order them any way you want - put yours on top if you want. Just duplicate the first entry and put your informa- tion into the duplicated version. 

 

  
There you have it - your new template is now registered in the list and it should be visible in the list when you opt to create a new site or new page for an existing site. The next steps involve the entire rework of the look and feel of your template.

It’s important that you type the file name exactly as it is. Also, the <key> is the name that will appear in the template selec- tion dropdown once your template is ready to go so use some- thing meaningful.

<key>Baby</key> <dict>

<key>displayName</key>
<string>About Me</string> <key>fileName</key>
<string>Baby About Me.webtemplate</string>

</dict>

<key>Graffiti</key> <dict>

<key>displayName</key>
<string>About Me</string>
<key>fileName</key>
<string>Graffiti About Me.webtemplate</string>

</dict>

 

 
##Step 3 - Replacing Graphics

Since we’ve copied an existing template, we can’t just make any kind of back- ground or header image. They can look like whatever you want, but they need to follow the basic dimensions of the graphics that already exist in the template. Some templates have graphics that other templates don’t so Know Thy Template first be- fore going forward.

In the Freestyle template, for example, we have three background images, one header image, one header/body separator image and a variety of supporting im- ages. You can change one, some or all of these, but they need to retain the original dimensions for the most part - this isn’t necessarily true, and those of you who know what you’re doing understand this, but I say this for the noobs who shouldn’t be doing this, but will anyway.

###Redesign Your Images

When I created my custom template using Freestyle, I opened up all three back- grounds, the header image, the separator image and a few of the supporting im- ages like the parchment paper. I re-colored, added textures, painted here and there - basically redesigned the images as I wanted. Then I saved them to a temporary folder. I used the exact same file names that the Freestyle template used.

This isn’t necessary, but it’s convenient later. Note that you might have to dig around in the different page folders to find all the graphics you want to alter. Not every page uses every image.

 

 
###Paste Your New Images into Your Template

Once you’re done redesigning any images, it’s time to copy them from your temp folder and paste them into the actual template files. If you remember, these were located here:

/iWeb/Contents/Resources/English.lproj/Templates/

Technically they’re inside each of the page folders at that location, but you should already know that.

7. Select all of your newly designed images and copy them in one shot.
8. Now open up your template files one by one (for each page) and paste your

new versions on top of the existing ones. Click Replace when prompted.

That’s it for graphics! Now when you start a new site or new page using your tem- plate, you should see your graphics and not those from the original template you duplicated! Cool :)

 

 
##Step 4 - Make New Thumbnails

I’m guessing out of excitement you’ve already tried to build a new page or site us- ing your template and you’re wondering why the preview images still show the original template’s look and feel. Well, it’s because you have to go and make those little thumbnail images yourself and place them into their respective thumbs folders in each template file. Then you’ll see them show up in preview.

The first thing you need to do is build a dummy site using your new template. Cre- ate one of all six page types and change any pictures on the pages if you want be- cause we’re about to take screenshots of each page. These will become your thumbnail images.

###Suggested Process

You can do this any way you want, but I’ll tell you what I did and you can decide from there. I went into each template file (for my new Graffiti template) one by one and found the thumbs folder. In there I found the original thumbnail image and I opened it in Photoshop. Next I went to my dummy iWeb site and took a screenshot of the page for which I was creating a thumbnail. Then I pasted the screenshot into my Photoshop file, resized it to fit the original dimensions, and saved it right back into its folder. I repeated this for each of the six pages. I was sure to save it in its original format also, TIFF. After this was done, I had six new thumbnail images and all of them were displaying properly in the template selection window!

page12image15216.png
 

 
##Step 5 - Conquer The XML

Okay you made it this far - hopefully you’ve gained enough excitement to take on the last challenge. However, maybe you’re just happy you got the first four steps. Either way, decide whether editing XML is really worth the changes it will provide you. If you stuck close to the colors and style of the original template, editing the XML just to make slight changes likely won’t be worth the effort. However, if your new look and feel doesn’t match the original fonts or font colors at all, then you have no choice, you must carry on :)

 

 
#Last Chance to Save Yourself!

Well this is it... this is the last piece you have to monkey with to get a totally customized, unique iWeb template for yourself. The XML files are not pretty - they’re pretty ugly. My eyes have been hurting for two days after staring at them and breaking them down into rec- ognizable chunks. However, I’m happy with the results and frankly, after the initial work, it gets easier and easier to find and change what you want. Just leave yourself comments so next time you cre- ate a template, you can go right to point you need using the Find function of your favorite code editor.

Rather than give you explicit instructions about how to change things in these files, I’m going to give you strategy because realistically, unless you chose the Freestyle template, your XML file is going to contain entirely different data. Everything from styles to text to layout information is in these files and they’re as unique as each template. So, I’ll tell you the basic method for finding, identifying and changing stuff that I used and you can work from there in a way that makes sense for you.

##Loading the XML Files

The files, if you’ve forgotten, are located within each of the template files you cre- ated for each of the page types. So for instance, go into the About Me folder, find your template, show the package contents and you’ll find it in there... index.xml.gz. First make a back-up of this file and tuck it away somewhere. Next, double-click the file and reveal the actual XML file.

Before you can even begin choking at the sight of this thing, you need to open it up in a code editor. I would not attempt to edit this file in TextEdit or any other appli- cation that doesn’t colorize code elements. You will lose your eyesight if you do.

However, I was not able to simply open it up in my favorite editor - the application crashed because all of the code is technically on one line! Crazy. At any rate, I first opened the file in TextEdit and then copied over big chunks of it (10-20 pages worth) at a time, each time inserting the chunks on new lines. I closed the original file, saved the new one in my code editor, closed it out and then copied the new version back into the template package. Then I reopened it in my editor from its native location... this just makes it easier when saving progress. Hope you followed all that.

 

 
##Anatomy of An XML File

###And other things you don’t want to know...

I’ll mention a few general things here that should make your time in this file more convenient. First don’t try to go formatting everything so you can read it more eas- ily. Realistically there’s not much you really have to change in these files so wasting your time formatting the file is exactly that, a major waste of time.

Second, for those who don’t know XML, it’s like HTML except that you get to make the tags so you’ll see a lot of weird things in these brackets < >. No worries, you really don’t need to care. However, it’s good to know that XML uses similar rules for opening and closing tags. For example <sf:fontSize> and </sf:fontSize>. Look fa- miliar? Good. The other thing, and your colorizing code editor will demonstrate this for you, is that the values are always in quotes and will be a different color than the tags themselves. I bet you can guess what font size has been specified here...

<sf:fontSize><sf:number sfa:number="72" sfa:type="f"/></sf:fontSize>
And did you need to know XML to determine that? Nope. Most of this is going to be clever searching and pure logic so here we go.

 

 
#What’s the first thing you don’t like?

###Have you successfully opened an XML file? If so, note which one, but for this conversation, I’ll reference the Welcome page.

Now as far as changing things, it really doesn’t matter where you start or what you start with, but just make sure it’s something you want to change. You might want to change the font, the font size, the color, the line spacing (height), capitalization or some other property of a given element. Regardless, find the element on the Welcome page and note its current properties. For the Freestyle template, the tem- plate I based my new template on, the header font is Portago and the font size is 70. So the first thing I’m going to do is use (command + F) to find the word Port- ago - can’t be too many of those in there!

##Your First Stab at Breaking the XML Apart

Well when you find the first instance of Portago in your file, it will be in a mess of code so here are some general guidelines to help you break it up and see it more clearly.

If you’re changing fonts and font colors, you’ll likely be looking for a big block of text in the XML code that looks like the following and if you look closely, you’ll see our font name in there, PortagoITCTT.

 

 
<sf:paragraphstyle sfa:ID="SFWPParagraphStyle-0" sf:name="Body"
sf:ident="paragraph-style-5"><sf:property-map><sf:defaultTabStops/><sf:underl
ineWidth/><sf:tabs/><sf:leftIndent/><sf:paragraphFill><sf:null/></sf:paragrap
hFill><sf:lineSpacing><sf:linespacing sfa:ID="SFWPLineSpacing-0" sf:amt="1.2"
sf:mode="relative"/></sf:lineSpacing><sf:paragraphBorders/><sf:italic/><sf:ba
selineShift/><sf:tocStyle><sf:null/></sf:tocStyle><sf:fontName><sf:string
sfa:string="PortagoITCTT"/></sf:fontName><sf:fontColor><sf:color
xsi:type="sfa:calibrated-rgb-color-type" sfa:r="0.27450981736183167"
sfa:g="0.23529411852359772" sfa:b="0.23529411852359772"
sfa:a="1"/></sf:fontColor><sf:rightIndent/><sf:textBackground><sf:null/></sf:
textBackground><sf:outlineLevel/><sf:strikethru/><sf:outline/><sf:dropCap/><s
f:listStyle><sf:null/></sf:listStyle><sf:textShadow/><sf:capitalization/><sf:
ligatures/><sf:keepWithNext/><sf:keepLinesTogether/><sf:textBorders/><sf:stri
kethruWidth/><sf:widowControl/><sf:word_underline/><sf:followingParagraphStyl
e><sf:null/></sf:followingParagraphStyle><sf:superscript/><sf:effectColor><sf
:null/></sf:effectColor><sf:spaceBefore/><sf:firstLineIndent/><sf:bold/><sf:s
howInTOC/><sf:fontSize><sf:number sfa:number="70"
sfa:type="f"/></sf:fontSize><sf:hidden/><sf:underline/><sf:effect><sf:null/><
/sf:effect><sf:hyphenate/><sf:word_strikethrough/><sf:outlineColor><sf:null/>
</sf:outlineColor><sf:spaceAfter/><sf:language><sf:null/></sf:language><sf:tr
acking/><sf:decimalTab/><sf:strikethruColor><sf:null/></sf:strikethruColor><s
f:atsuFontFeatures/><sf:pageBreakBefore/><sf:kerning/><sf:paragraphStroke><sf
:null/></sf:paragraphStroke><sf:underlineColor><sf:null/></sf:underlineColor>
<sf:alignment/><sf:firstTopicNumber/></sf:property-map></sf:paragraphstyle>
Notice that the whole chunk of code starts with something called a paragraph style - this is typical for text styles, but not always the case. Here’s that same code for- matted across multiple lines so we can talk about the various properties:

 

 
<sf:paragraphstyle sfa:ID="SFWPParagraphStyle-0" sf:name="Body"
sf:ident="paragraph-style-5">
  <sf:property-map>
    <sf:defaultTabStops/>
    <sf:underlineWidth/>
    <sf:tabs/>
    <sf:leftIndent/>
    <sf:paragraphFill><sf:null/></sf:paragraphFill>
    <sf:lineSpacing><sf:linespacing sfa:ID="SFWPLineSpacing-0" sf:amt="1.2"
sf:mode="relative"/></sf:lineSpacing>
    <sf:paragraphBorders/>
    <sf:italic/>
    <sf:baselineShift/>
    <sf:tocStyle><sf:null/></sf:tocStyle>
    <sf:fontName><sf:string sfa:string="PortagoITCTT"/></sf:fontName>
    <sf:fontColor><sf:color xsi:type="sfa:calibrated-rgb-color-type"
sfa:r="0.27450981736183167" sfa:g="0.23529411852359772"
sfa:b="0.23529411852359772" sfa:a="1"/></sf:fontColor>
    <sf:rightIndent/>
    <sf:textBackground><sf:null/></sf:textBackground>
    <sf:outlineLevel/>
    <sf:strikethru/>
    <sf:outline/>
    <sf:dropCap/>
    <sf:listStyle><sf:null/></sf:listStyle>
    <sf:textShadow/>
    <sf:capitalization/>
    <sf:ligatures/>
    <sf:keepWithNext/>
    <sf:keepLinesTogether/>
    <sf:textBorders/>
    <sf:strikethruWidth/>
    <sf:widowControl/>
    <sf:word_underline/>
    <sf:followingParagraphStyle><sf:null/></sf:followingParagraphStyle>
    <sf:superscript/>
    <sf:effectColor><sf:null/></sf:effectColor>
    <sf:spaceBefore/>
    <sf:firstLineIndent/>
    <sf:bold/>
    <sf:showInTOC/>
    <sf:fontSize><sf:number sfa:number="70" sfa:type="f"/></sf:fontSize>
    <sf:hidden/>
    <sf:underline/>
    <sf:effect><sf:null/></sf:effect>
iWeb Template How-To

Last Updated February 26, 2006

 
    <sf:hyphenate/>
    <sf:word_strikethrough/>
    <sf:outlineColor><sf:null/></sf:outlineColor>
    <sf:spaceAfter/>
    <sf:language><sf:null/></sf:language>
    <sf:tracking/>
    <sf:decimalTab/>
    <sf:strikethruColor><sf:null/></sf:strikethruColor>
    <sf:atsuFontFeatures/>
    <sf:pageBreakBefore/>
    <sf:kerning/>
    <sf:paragraphStroke><sf:null/></sf:paragraphStroke>
    <sf:underlineColor><sf:null/></sf:underlineColor>
    <sf:alignment/>
    <sf:firstTopicNumber/>
  </sf:property-map>
</sf:paragraphstyle>
Now for most things you’ll change, you’ll first search for it using terms you already know... like the original font name for the Freestyle header and then you’ll over- write the value with the one you want. For example, I changed all instances of Port- ago to Impact in my new template. I also bumped up the font size to 72 for the header. You should be able to easily see these values in this code. If it’s easier for you to be sure of what you’re changing, format the code as shown in the second example, but be extremely careful when you do it or you will mess things up badly.

After I made changes to a style, I inserted a comment so I could identify it more easily next time.

<!-- Header Text Style --> or <!-- Nav Menu Rollover Color -->

 

 
##Other Properties You Might Want to Change

I found some common patterns when changing routine things and I’ll mention those here.

###Font Colors

Font colors are specified in RGB 100 values, but why they run the decimal point out so dang far is beyond me! Lord, look at these color specifications:

<sf:fontColor><sf:color xsi:type="sfa:calibrated-rgb-color-type"
sfa:r="0.27450981736183167" sfa:g="0.23529411852359772"
sfa:b="0.23529411852359772" sfa:a="1"/></sf:fontColor>
This isn’t necessary. The colors could have been specified using just two decimal places and if you use Code Line’s Art Directors Took Kit to find new colors for your- self, then two decimal places is what you’ll use. Running the decimal out like that will actually alter the color slightly, obviously, but is that really necessary for per- sonal web sites? It’s your site :)

<sf:fontColor><sf:color xsi:type="sfa:calibrated-rgb-color-type" sfa:r="0.27"
sfa:g="0.24" sfa:b="0.24" sfa:a="1"/></sf:fontColor>

###Make It Bold

If you want to make a font bold, overwrite the piece <sf:bold/> with: <sf:bold><sf:number sfa:number="1" sfa:type="i"/></sf:bold>

###Turn Off All-Caps

This was irritating to me - I turned off all the forced capitalization in the Freestyle template by overwriting this:

<sf:capitalization><sf:number sfa:number="1"
sfa:type="i"/></sf:capitalization>
with this:

<sf:capitalization/>
Notice what changed. When a property has a value, then an open and close tag is present with some other stuff in the middle. However, if you just want to turn off a property, simply get rid of everything but the first tag and add the slash / before the closing bracket.

###Turn Bold Off

Can you guess? <sf:bold/>

###Add Shadows To Text

Here’s the code that enables a shadow on your text. You can see properties for the shadow angle, the offset, the radius (called Blur in the interface) and opacity. You

 

 
can also specify a shadow color, but again, it doesn’t need to be 15 digits long - two will get the job done.

<sf:textShadow>
  <sf:shadow sfa:ID="SFRShadow-4" sf:angle="45" sf:offset="1" sf:radius="6"
sf:opacity="0.75">
  <sf:color xsi:type="sfa:calibrated-rgb-color-type" sfa:r="0.27"
sfa:g="0.23" sfa:b="0.23" sfa:a="1"/>
</sf:shadow>

###Set Line Height

In the code they call it line spacing, but the interface calls it Line. It’s on the text inspector panel. When I changed my body text from Palatino to Verdana, I also changed the line height from 1.1 to 1.2. When looking for these values, note that the actual value will be something like 1.10004935839. Again, I’m not sure why they did this, but a simple 1.1 or 1.2 will work just as well.

<sf:lineSpacing><sf:linespacing sfa:ID="SFWPLineSpacing-0" sf:amt="1.2"
sf:mode="relative"/></sf:lineSpacing>

#More later!

But if you just can’t wait, and you basically get what has to be done to change eve- rything else already, then the last step is to re-zip the index.xml file to index.xml.gz or index.xml.tgz. THAT’S IT - Whew! Create a new page with your template - or more realistically, see what you’ve missed and keep editing ;-)